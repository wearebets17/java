/**
 * @author  : Fajar Subhan
 * @desc    : UAP Pemrograman 2
 * @npm     : 202043500578
 * @help    : Nama Class Tolong di ganti jangan sama
 * */

/* Import Scanner Class */
import java.util.Scanner;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;


public class SchoolFees
{
    public static void main(String[] school)
    {
        /* ---------- start variable declaration ------------------- */
        int pengembangan,spp,kegiatan,raport,total;

        /**
         *
         * uk    = uang kegiatan
         * ur    = uang raport
         * up    = uang pengembangan
         * usp   = uang spp
         *
         * */
        int uk   = 3500000;
        int ur   = 500000;
        int up   = 3000000;
        int usp  = 1200000;

        /* Set Format Number */
        DecimalFormat        formatID   = (DecimalFormat) DecimalFormat.getInstance();
        DecimalFormatSymbols fd         = new DecimalFormatSymbols();
        fd.setGroupingSeparator('.');
        formatID.setDecimalFormatSymbols(fd);

        Scanner inputUser   = new Scanner(System.in);

        /* ---------- end variable declaration ------------------- */

        System.out.println("            RA.Al-Karomah Bogor");
        System.out.println("Silahkan pilih bayaran yang ingin dibayar !!!");
        System.out.println("1) . PENGEMBANGAN");
        System.out.println("2) . SPP");
        System.out.println("3) . KEGIATAN");
        System.out.println("4) . RAPORT");

        System.out.println("--------------------------------------------------------------");
        // Pengembangan
        System.out.print("Apakah ingin membayar PENGEMBANGAN (pilih 1 jika ya,pilih 2 jika tidak) : ");
        pengembangan = inputUser.nextInt();

        // SPP
        System.out.print("Apakah ingin membayar SPP (pilih 1 jika ya,pilih 2 jika tidak) : ");
        spp = inputUser.nextInt();

        // Uang Kegiatan
        System.out.print("Apakah ingin membayar UANG KEGIATAN (pilih 1 jika ya,pilih 2 jika tidak) : ");
        kegiatan = inputUser.nextInt();

        // Uang Raport
        System.out.print("Apakah ingin membayara RAPORT (pilih 1 jika ya,pilih 2 jika tidak) :");
        raport = inputUser.nextInt();
        System.out.println("--------------------------------------------------------------");


        /* Proses Data */

        /**
         * Ketentuan no 1
         * A.up,usp,uk,ur dibayarkan |
         * B.up,usp,ur    dibayarkan | uk     tidak bayar
         * C.up,usp,uk    dibayarkan | ur     tidak bayar
         * D.up,usp       dibayarkan | uk,ur  tidak bayar
         * */
        if(pengembangan == 1)
        {
            if(spp == 1)
            {
                if(kegiatan == 1)
                {
                    // kondisi 1.A output
                    if(raport == 1)
                    {
                        System.out.println("Terima kasih telah melunasi semua");
                        total = up + usp + uk + ur;
                        System.out.println("Total yang sudah dibayarkan = Rp. " + total);
                    }
                    // kondisi 1.C output
                    else if(raport == 2)
                    {
                        System.out.println("Uang raport belum Rp. " + formatID.format(ur));
                        total = usp + up + uk;
                        System.out.println("Total yang sudah dibayarkan = Rp. " + total);
                    }
                }
                else if(kegiatan == 2)
                {
                    // kondisi 1.B output
                    if(raport == 1)
                    {
                        System.out.println("Uang kegiatan belum Rp. " + formatID.format(uk));
                        total = up + usp + ur;
                        System.out.println("Total yang sudah dibayarkan = Rp. " + total);
                    }
                    // kondisi 1.D output
                    else if(raport == 2)
                    {
                        System.out.println("Uang kegiatan Rp. " + formatID.format(uk) + " dan raport belum Rp. " + formatID.format(ur));
                        total = up + usp;
                        System.out.println("Total yang sudah dibayarkan = Rp. " + total);
                    }
                }
            }

            /**
             *  Ketentuan no 3
             *  A.up,uk,ur  dibayarkan  | usp       tidak bayar
             *  B.up,ur     dibayarkan  | usp,uk    tidak bayar
             *  C.up,uk     dibayarkan  | usp,ur    tidak bayar
             *  D.up       dibayarkan   | usp,uk,ur tidak bayar
             *
             * @Fyi : perhatikan soal nomer 3.D atau urutan terakhir di soal ketentuan no 3
             * disitu tertulis total yang sudah dibayarkan = pengembangan, padahal kondisi disitu
             * uang pengembangan tidak ingin dibayarkan.
             * */
            else if(spp == 2)
            {
                if(kegiatan == 1)
                {
                    // Kondisi 3.A output
                    if(raport == 1)
                    {
                        System.out.println("Uang spp belum Rp. " + formatID.format(usp));
                        total = up + uk + ur;
                        System.out.println("Total yang sudah dibayarkan = Rp. " + total);
                    }
                    // Kondisi 3.C output
                    else if(raport == 2)
                    {
                        System.out.println("Uang spp Rp. " + formatID.format(usp) + " dan raport Rp. " + formatID.format(ur));
                        total = up + uk;
                        System.out.println("Total yang sudah dibayarkan = Rp. " + total);
                    }
                }
                else if(kegiatan == 2)
                {
                    // Kondisi 3.B output
                    if(raport == 1)
                    {
                        System.out.println("Uang spp Rp. " + formatID.format(usp) + " dan kegiatan belum Rp. " + formatID.format(uk));
                        total = up + ur;
                        System.out.println("Total yang sudah dibayarkan = Rp. " + total);
                    }
                    // Kondisi 3.D
                    else if(raport == 2)
                    {
                        System.out.println("Uang spp Rp. " + formatID.format(usp) + " , uang kegiatan Rp. " + formatID.format(uk) + " dan raport Rp. " + formatID.format(ur) + " belum dibayarkan");
                        total = up;
                        System.out.println("Total yang sudah dibayarkan Rp. " + total);
                    }

                }
            }
        }

        /**
         * Ketentuan no 2
         * A.up         tidak bayar     |   usp,uk,ur   dibayarkan
         * B.up,uk      tidak bayar     |   usp,ur      dibayarkan
         * C.up,ur      tidak bayar     |   usp,uk      dibayarkan
         * D.up,uk,ur   tidak bayar     |   usp         dibayarkan
        * */
        else if(pengembangan == 2)
        {
            if(spp == 1)
            {
                if(kegiatan == 1)
                {
                    // Kondisi 2.A output
                    if(raport == 1)
                    {
                        System.out.println("Uang pengembangan belum Rp. " + formatID.format(up));
                        total = usp + uk + ur;
                        System.out.println("Total yang sudah dibayarkan = Rp. " + total);
                    }
                    // Kondisi 2.C output
                    else if(raport == 2)
                    {
                        System.out.println("Uang pengembangan Rp. " + formatID.format(up) + " dan raport belum Rp. " + formatID.format(ur));
                        total = usp + uk;
                        System.out.println("Total yang sudah dibayarkan = Rp. " + total);
                    }
                }
                else if(kegiatan == 2)
                {
                    // Kondisi 2.B output
                    if(raport == 1)
                    {
                        System.out.println("Uang pengembangan Rp. " + formatID.format(up) + " dan kegiatan belum Rp. " + formatID.format(uk));
                        total = usp + ur;
                        System.out.println("Total yang sudah dibayarkan = Rp. " + total);
                    }
                    // Kondisi 2.D output
                    else if(raport == 2)
                    {
                        System.out.println("Uang pengembangan Rp. " + formatID.format(up) + " , kegiatan Rp. " + formatID.format(uk) + " dan raport belum Rp. " + formatID.format(ur));
                        total = usp;
                        System.out.println("Total yang sudah dibayarkan = Rp. " + total);
                    }
                }
            }

            /**
             * Ketentuan no 4
             * A.up,usp        tidak bayar |   uk,ur  dibayarkan
             * B.up,usp,uk     tidak bayar |   ur     dibayarkan
             * C.up,usp,ur     tidak bayar |   uk     dibayarkan
             * D.up,usp,uk,uk  tidak bayar |
             * */
            else if(spp == 2)
            {
                if(kegiatan == 1)
                {
                    // Kondisi 4.A output
                    if(raport == 1)
                    {
                        System.out.println("Uang pengembangan Rp. " + formatID.format(up) + " dan spp belum Rp. " + formatID.format(usp));
                        total = uk + ur;
                        System.out.println("Total yang sudah dibayarkan = Rp. " + total);
                    }
                    // Kondisi 4.C output
                    else if(raport == 2)
                    {
                        System.out.println("Uang pengembangan Rp. " + formatID.format(up) + " , spp Rp. " + formatID.format(usp) + " dan raport belum Rp. " + formatID.format(ur));
                        total = uk;
                        System.out.println("Total yang sudah dibayarkan = Rp. " + total);
                    }
                }
                else if(kegiatan == 2)
                {
                    // Kondisi 4.B output
                    if(raport == 1)
                    {
                        System.out.println("Uang pengembangan Rp. " + formatID.format(up) + " , spp Rp. " + formatID.format(usp) + " dan kegiatan belum Rp. " + formatID.format(uk));
                        total = ur;
                        System.out.println("Total yang sudah dibayarkan = Rp. " + total);
                    }
                    // Kondisi 4.D output
                    else if(raport == 2)
                    {
                        System.out.println("HARAP LUNASI SEGERA !!!!");
                    }
                }
            }
        }
    }
}
