/**
 * masukan object scanner yang dimiliki oleh java
 * untuk mengambil inputan dari keyboard user
 *
 * */
import java.util.Scanner;

public class LuasPersegiPanjang
{
    /**
     * @author fajar subhan
     * @return int | string
     * @description calculate the area of the rectangle
     *
     * */
    public static void main(String[] data)
    {
        /**
         * @notes : luas persegi panjang adalah L = p x lcalculate the area of the rectangle
         *
         * */
         int panjang;
         int lebar;
         int luas;

         // Create instance object scanner
         Scanner inputUser = new Scanner(System.in);

        /**
         * @fyi         : ambil data inputan user kedalam variable panjang dan lebar
         * nextInt()    : mengambil data berupa number atau integer
         * next()       : berupa data inputan String / Text
         *
         * */
        System.out.print("Masukan panjang : ");
        panjang = inputUser.nextInt();

        System.out.print("Masukan lebar : ");
        lebar   = inputUser.nextInt();

        // proses perhitungan
        luas = panjang * lebar;
        System.out.println("Luas persegi panjang = " + luas);
    }
}
