/*
* @Authors  : Fajar Subhan
* @Programs : Calculators Basic Simple With If Else If
* */

import java.util.Scanner;

public class Calculators_V1
{
    public static void main(String[] args)
    {
        // Start The Declaration Variable

        float x,y,result;
        char  operator;
        Scanner user_input;

        // End The Declaration Variable

        // Start Assignment Data On User Input

        // Create Object Scanner Instances
        user_input = new Scanner(System.in);

        System.out.print("Input Value x = ");
        x = user_input.nextFloat();

        System.out.print("Input Operator = ");
        operator = user_input.next().charAt(0);

        System.out.print("Input Value y = ");
        y = user_input.nextFloat();

        System.out.println("Input User : " + x + " " + operator + " " + y);

        result = 0;
        // End Assignment Data On User Input

        /* Operator + , * , / , - */
        if (operator == '+')
        {
            result = x + y;
        }
        else if (operator == '*')
        {
            result = x * y;
        }
        else if (operator == '/')
        {
            if( (x == 0) || (y == 0) )
            {
                System.out.println("!!! === Infinity Value === !!!");
            }
            else
            {
                result = x / y;
            }
        }
        else if (operator == '-')
        {
            result = x - y;
        }
        else
        {
            System.out.println("Operator Not Found !!!");
        }

        // Output Result
        System.out.println("Output Result = " + result);
    }
}
