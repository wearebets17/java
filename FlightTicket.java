/**
 * @author  : Fajar Subhan
 * @desc    : UAP Pemrograman 2
 * @npm     : 202043500578
 * @help    : Nama Class Tolong di ganti jangan sama 
 * */

/* Import Scanner Class */
import java.util.Scanner;

public class FlightTicket
{
    public static void main(String[] flight)
    {
        /* ---------- start variable declaration ------------------- */
        int selectPlane;
        int totalTicket;
        int price = 0;
        String namePlane = "";
        int discount = 0;
        int total = 0;

        Scanner inputUser = new Scanner(System.in);
        /* ---------- end variable declaration ------------------- */

        /* -------------- start header ---------------- */
        System.out.println("            BELITIKET DOT COM");
        System.out.println("");

        System.out.println("Silahkan pilih nama pesawat yang diinginkan !!!");
        System.out.println("1) . Garuda");
        System.out.println("2) . Lion");
        System.out.println("3) . Citilink");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        /* Select plane */
        System.out.print("Silahkan pilih ( [1] atau [2] atau [3] ) : ");
        selectPlane = inputUser.nextInt();

        /* Input total ticket */
        System.out.print("Masukan jumlah tiket yang ingin di beli : ");
        totalTicket = inputUser.nextInt();
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        /* -------------- end header ---------------- */

        /* Proses data */

        // Ketentuan 1 dan 2 [Garuda = 1]
        if(selectPlane == 1)
        {
            // Ketentuan 1
            if(totalTicket >= 100)
            {
                price       = 400000;
                namePlane   = "Garuda-Diskon 50%";
                discount    = 200000;
                total       = (price-discount)*totalTicket;
            }
            // ketentuan 2
            else if(totalTicket < 100)
            {
                price       = 400000;
                namePlane   = "Garuda-Tidak ada diskon";
                total       = price*totalTicket;
            }
        }
        // Ketentuan 3 dan 4 [Lion = 2]
        else if(selectPlane == 2)
        {
            // Ketentuan 3
            if(totalTicket >= 100)
            {
                price       = 200000;
                namePlane   = "Lion-Diskon 50%";
                discount    = 100000;
                total       = (price-discount)*totalTicket;
            }
            // Ketentuan 4
            else if(totalTicket < 100)
            {
                price       = 200000;
                namePlane   = "Lion-Tidak ada diskon";
                total       = price*totalTicket;
            }
        }
        // Ketentuan 5 dan 6 [Citilink = 3]
        else if(selectPlane == 3)
        {
            // Ketentuan 5
            if(totalTicket >= 100)
            {
                price       = 300000;
                namePlane   = "Citilink-Dikon 50%";
                discount    = 150000;
                total       = (price-discount)*totalTicket;
            }
            // Ketentuan 6
            else if(totalTicket < 100)
            {
                price       = 300000;
                namePlane   = "Citilink-Tidak ada diskon";
                total       = price*totalTicket;
            }
        }

        /* Output Print */
        System.out.println("Nama Pesawat : " + namePlane);
        System.out.println("Harga Tiket  : " + price);
        System.out.println("Diskon       : " + discount);
        System.out.println("Total        : " + total);

    }
}
